from celery import Celery

app = Celery('tasks', backend='rpc://', broker='pyamqp://guest@192.168.0.24//')

@app.task
def add(x, y):
    return x + y
