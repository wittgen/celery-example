import celery
import time
app = celery.Celery('tasks', backend='rpc://', broker='pyamqp://guest@192.168.0.24//')
RCEs=("rce1","rce2")
task_list=list()

for rce in RCEs:
    print("Start ",rce)
    task=app.send_task('tasks.add',args=(4,4),kwargs={},queue=rce)
    task_list.append(task)

while True:
    done = True
    for t in task_list: 
        done = done and t.ready()
        time.sleep(0.1)
    if done: break
for t in task_list:
    print(t.get())   

